package com.jettada.week11;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        Bat bat1 = new Bat("Batman");
        bat1.eat();
        bat1.sleep();
        bat1.takeoff();
        bat1.fly();
        bat1.landing();

        Fish fish1 = new Fish("Nimo");
        fish1.eat();
        fish1.sleep();
        
        Plane plane1 = new Plane("Boing", "Boing Engine");

        Submarine submarine1 = new Submarine("Prayut", "Prayut Engine");

        Crocodile crocodile1 = new Crocodile("Charawan");
        crocodile1.eat();
        crocodile1.sleep();
        crocodile1.crawl();
        crocodile1.swim();

        Bird bird1 = new Bird("Blue");
        bird1.eat();
        bird1.sleep();

        Snake snake1 = new Snake("Nakee");
        snake1.eat();
        snake1.sleep();

        Rat rat1 = new Rat("Jerry");
        rat1.eat();
        rat1.sleep();

        Human human1 = new Human("Benz");
        human1.eat();
        human1.sleep();

        Dog dog1 = new Dog("Moji");
        dog1.eat();
        dog1.sleep();

        Cat cat1 = new Cat("Tom");
        cat1.eat();
        cat1.sleep();

        Walkable[] walkablesObject = {rat1,human1,dog1,cat1,bird1};
        for(int i = 0; i< walkablesObject.length; i++){
            walkablesObject[i].walk();
            walkablesObject[i].run();
        }

        Crawlable[] crawlablesObject = {snake1,crocodile1};
        for(int i = 0; i < crawlablesObject.length ; i++){
            crawlablesObject[i].crawl();
        }

        Swimable[] swimablesObject = {fish1,crocodile1,submarine1};
        for(int i = 0 ; i < swimablesObject.length ; i ++){
            swimablesObject[i].swim();
        }

        Flyable[] flyablesObject = {bat1,bird1,plane1};
        for(int i = 0; i <flyablesObject.length; i++){
            flyablesObject[i].takeoff();
            flyablesObject[i].fly();
            flyablesObject[i].landing();
        }

    }
}

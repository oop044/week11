package com.jettada.week11;

public interface Flyable {
    public void fly();
    public void landing();
    public void takeoff();
}
